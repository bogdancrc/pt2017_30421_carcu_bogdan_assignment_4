package PT2017.demo.Bank;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable{

	private static final long serialVersionUID = 1L;

	public static int ACCOUNT_NO = 0;
	
	private int id;
	private LocalDate creationDate;
	private String ownerName;
	private double sum;
	private AccountType type;
	
	public abstract void addSum(double sum);
	public abstract double withdraw();
	public abstract String toString();
	
	public String getOwnerName() {
		return ownerName;
	}
	
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	
	public LocalDate getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}
	
	public double getSum() {
		return sum;
	}
	
	public void setSum(double sum) {
		
		this.sum = sum;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public AccountType getType() {
		return type;
	}
	
	public void setType(AccountType type) {
		this.type = type;
	}
}
