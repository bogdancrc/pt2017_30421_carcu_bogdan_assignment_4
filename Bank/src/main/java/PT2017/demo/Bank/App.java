package PT2017.demo.Bank;

import gui.Controller;
import gui.View;

/**
 * Main Application Launcher
 *
 */
public class App {
    public static void main( String[] args ) {
    	
    	Bank bank = BankDatabase.loadData();
    	new Controller(new View(), bank);
  
    }
}
