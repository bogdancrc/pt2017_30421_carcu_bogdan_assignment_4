package PT2017.demo.Bank;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @invariant isWellFormed()
 */
public class Bank implements BankProc, Serializable{

	private static final long serialVersionUID = 1L;
	
	private Map<Person, List<Account>> map;
	
	public Bank() {
		
		map = new HashMap<Person, List<Account>>();
		
		assert isWellFormed();
	}
	
	protected boolean isWellFormed() {
		
		return !map.containsKey(null) && !map.containsValue(null);
		
	}

	public void addPerson(Person p) {
		
		assert p != null;
		assert isWellFormed();
		int preSize = map.size();
	
		map.put(p, new ArrayList<Account>());
		
		assert map.size() == preSize + 1;
		assert isWellFormed();
	}

	public boolean removePerson(Person p) {
		
		assert p != null;
		assert map.containsKey(p) == true;
		assert isWellFormed();
		int preSize = map.size();
		
		map.remove(p);
		boolean result = true;
		
		assert map.size() == preSize - 1;
		assert result == true;
		assert isWellFormed();
		
		return result;
	}
	
	public void addAccount(Person p, AccountType type) {
		
		assert p != null  && type != null;
		assert isWellFormed();
		
		List<Account> accounts = map.get(p);
		int preSize = accounts.size();
		
		if(type == AccountType.SAVING) {
			Account newAcc = new SavingAccount(p.getName());
			newAcc.addObserver(p);
			accounts.add(newAcc);
		}
		else {
			Account newAcc = new SpendingAccount(p.getName());
			newAcc.addObserver(p);
			accounts.add(newAcc);
		}
		
		map.replace(p, accounts);
		
		assert preSize + 1 == accounts.size();
		assert isWellFormed();
		
	}

	public boolean deleteAccount(Person p, int accountId) {
		
		assert p != null  && accountId > 0;
		assert isWellFormed();
				
		List<Account> accounts = map.get(p);
		Account c = null;
		
		for(Account account : accounts)
			if(account.getId() == accountId) {		
				c = account;
				break;	
			}
		
		int preSize = accounts.size();
		boolean result = accounts.remove(c);
		map.replace(p, accounts);
	
		assert preSize == accounts.size() + 1;
		assert result == true;
		assert isWellFormed();
		return result;
	}

	public void deposit(Person p, int accountId, double sum) {
		
		assert p != null && accountId > 0 && sum > 0;
		assert isWellFormed();
		
		List<Account> accounts = map.get(p);
		double postSum = 0;
		
		for(Account account : accounts)
			if(account.getId() == accountId) {
				
				account.addSum(sum);
				postSum = account.getSum();
				break;
			}

		map.replace(p, accounts);
		
		assert postSum >= sum;
		assert isWellFormed();
	}

	public double withdraw(Person p, int accountId, double sum) {
		
		assert p != null && accountId > 0 && sum >= 0;
		assert isWellFormed();
		
		List<Account> accounts = map.get(p);
		double balance = 0;
		
		for(Account account : accounts)
			if(account.getId() == accountId) {
				if(account.getType() == AccountType.SAVING)
					balance = ((SavingAccount)account).withdraw();
				else
					balance  = ((SpendingAccount)account).withdraw(sum);
				
				break;
			}
		
		map.replace(p, accounts);
		
		assert balance >= 0 ;
		assert isWellFormed();
		return balance;
	}

	public String generateReport(Person p, int accountId) {

		assert p != null && accountId > 0;
		assert isWellFormed();
		
		List<Account> accounts = map.get(p);
		String result = "";
		
		for(Account account : accounts) 
			if(account.getId() == accountId) {
				result += p;
				result += "\n-----------------\n";
				result += account;
				break;
			}
		
		assert isWellFormed();
		return result;
	}
	
	public Person findById(int id) {
		
		assert id > 0;
		assert isWellFormed();
		Person found = null;
		
		for(Person p : map.keySet())
			if(p.getId() == id) {
				found = p;
				break;
			}
		
		assert isWellFormed();
		return found;
	}
	
	public Map<Person, List<Account>> getMap() {
		
		return map;
		
	}
	
}
