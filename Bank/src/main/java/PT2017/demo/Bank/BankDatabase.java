package PT2017.demo.Bank;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class BankDatabase {
	
	private static final String PATH = "E:\\Faculty\\SEM2\\PT3\\PT2017_30421_Carcu_Bogdan\\Bank\\bankdata.ser";
	
	private BankDatabase() {
		
	}
	
	public static void saveData(Bank bank) {
		
		try {
	         FileOutputStream outputFile = new FileOutputStream(PATH);
	         ObjectOutputStream outStream = new ObjectOutputStream(outputFile);
	         outStream.writeObject(bank);
	         outStream.close();
	         outputFile.close();
	         System.out.println("Serialized bank data is saved in " + PATH);
		
		} catch(IOException e) {
			
			e.printStackTrace();
		}
	}	
	
	public static Bank loadData() {
		
		Bank bank = null;
	      
		try {
	         FileInputStream inputFile = new FileInputStream(PATH);
	         ObjectInputStream inStream = new ObjectInputStream(inputFile);
	         bank = (Bank) inStream.readObject();
	         inStream.close();
	         inputFile.close();
	         
	      }catch(Exception e) {
	      
	    	  e.printStackTrace();
	      }
		
		return bank;
	}
	
}
