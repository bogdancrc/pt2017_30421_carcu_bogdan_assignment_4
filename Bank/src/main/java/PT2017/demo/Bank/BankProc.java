package PT2017.demo.Bank;

public interface BankProc {
	
	/**
	 * @pre p != null
	 * @post map.size() = map.size()@pre + 1
	 */
	public void addPerson(Person p);
	
	/**
	 * @pre p != null && map.containsKey(p) == true
	 * @post map.size() == map.size()@pre - 1 && @result == true
	 */
	public boolean removePerson(Person p);
	
	/**
	 * @pre p != null && type != null
	 * @post accounts.size() == accounts.size()@pre + 1
	 */
	public void addAccount(Person p, AccountType type);
	
	/**
	 * @pre p != null  && accountId > 0
	 * @post accounts.size() == accounts.size()@pre - 1 && @result == true
	 */
	public boolean deleteAccount(Person p, int accountId);
	
	/**
	 * @pre sum > 0 && p != null && accountId > 0
	 * @post postSum >= sum
	 */
	public void deposit(Person p, int accountId, double sum);
	
	/**
	 * @pre sum < getSum() && p != null && accountId > 0
	 * @post getSum() >= 0
	 */
	public double withdraw(Person p, int accountId, double sum);
	
	/**
	 * @pre p != null && accountId > 0
	 * @post @nochange
	 */
	public String generateReport(Person p, int accountId);
	
	/**
	 * @pre id > 0 
	 * @post @nochange
	 */
	public Person findById(int id);
		

}
