package PT2017.demo.Bank;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Observer, Serializable{

	private static final long serialVersionUID = 1L;
	
	private static int PERSON_NO = 0;
	
	private final int id;
	private final String name;
	private final int age;
	private final String address;
	
	public Person(String name, int age, String address) {
		
		PERSON_NO++;
		this.id = PERSON_NO;
		this.name = name;
		this.age = age;
		this.address = address;
		
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public String getAddress() {
		return address;
	}
	
	public String toString() {
		
		return "Id: " + id + ", Name: " + name + ", Age: " + age + ", Address: " + address;
		
	}
	
	public void update(Observable o, Object arg) {
		
		if(o instanceof SavingAccount)
			System.out.println((arg) + " operation of " + ((SavingAccount)o).getOwnerName() + 
					" in the Saving Account #" + ((SavingAccount)o).getId() +" was successful!");
		else
			System.out.println((arg) + " operation of " + ((SpendingAccount)o).getOwnerName() + 
					" in the Spending Account #" + ((SpendingAccount)o).getId() +  " was successful!");
	}
	
	@Override
	public int hashCode() {
		
		int result = 17;
        result = 31 * result + id;
        result = 31 * result + name.hashCode();
        result = 31 * result + age;
        result = 31 * result + address.hashCode();
        
        return result;
		
	}
	
	@Override
	 public boolean equals(Object obj) {

        if (obj == this)
        	return true;
        
        if ((obj instanceof Person) == false) 
            return false;

        Person p = (Person) obj;

        return p.id == id && p.name.equals(name) && p.age == age && p.address.equals(address);
    }
	
}
