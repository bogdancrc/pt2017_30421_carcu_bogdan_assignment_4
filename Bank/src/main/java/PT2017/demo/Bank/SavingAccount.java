package PT2017.demo.Bank;

import java.time.LocalDate;
import java.time.Period;

public class SavingAccount extends Account{

	private static final long serialVersionUID = 1L;
	
	private static final double INTEREST = 0.1;
	private static final double MIN_SUM = 2000.0;
	
	public SavingAccount(String name) {
		
		this.setCreationDate(LocalDate.now());
		this.setOwnerName(name);
		ACCOUNT_NO++;
		this.setId(ACCOUNT_NO);
		this.setType(AccountType.SAVING);
	}
	
	@Override
	public void addSum(double sum) {
		
		if(sum >= MIN_SUM) {
			this.setSum(sum);
			setChanged();
			notifyObservers("Deposit");
		}
		else
			throw new IllegalArgumentException("Sum is too low!");
		
	}

	public double withdraw() {
		
		double passedTime = Period.between(this.getCreationDate(), LocalDate.now()).getMonths();
		
		if(passedTime == 0) {
			
			double result = getSum();
			this.setSum(0);
			setChanged();
			notifyObservers("Withdrawal");
			System.out.println("After computing based on INTEREST, the sum is: " + result + "$");
			return result;
		}
			
		double result = this.getSum() * (passedTime / 12.0) * INTEREST;
		this.setSum(0);
		setChanged();
		notifyObservers("Withdrawal");
		
		System.out.println("After computing based on INTEREST, the sum is: " + result + "$");
		return result;
	}

	@Override
	public String toString() {
		
		return "Id: " + getId() + ", Account type: Saving Account, Owner: " + getOwnerName() + ", was created at: " +
				getCreationDate() + ", and has a deposit of: " + getSum() + "$";
		
	}
	
}
