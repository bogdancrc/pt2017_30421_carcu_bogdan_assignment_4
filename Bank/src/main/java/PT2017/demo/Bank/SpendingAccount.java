package PT2017.demo.Bank;

import java.time.LocalDate;

public class SpendingAccount extends Account{

	private static final long serialVersionUID = 1L;

	public SpendingAccount(String name) {
		
		this.setCreationDate(LocalDate.now());
		this.setOwnerName(name);
		ACCOUNT_NO++;
		this.setId(ACCOUNT_NO);
		this.setType(AccountType.SPENDING);
	}
	
	public double withdraw(double sum) {
		
		if(sum <= getSum()) {
			
			setSum(getSum() - sum);
			setChanged();
			notifyObservers("Withdrawal");
			return sum;
			
		} 
		
		throw new IllegalArgumentException("Cannot withdraw. Sum is too big.");
	}
	
	public double withdraw() {
		
		return this.withdraw(getSum());
		
	}
	
	@Override
	public void addSum(double sum) {
		
		setChanged();
		notifyObservers("Deposit");
		this.setSum(sum + this.getSum());
		
	}

	@Override
	public String toString() {
		
		return "Id: " + getId() + ", Account type: Spending Account, Owner: " + getOwnerName() + ", was created at: " +
				getCreationDate() + ", and has a deposit of: " + getSum() + "$";
	}

}
