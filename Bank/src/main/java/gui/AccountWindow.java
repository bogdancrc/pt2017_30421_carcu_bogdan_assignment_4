package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import PT2017.demo.Bank.AccountType;
import PT2017.demo.Bank.Bank;
import PT2017.demo.Bank.Person;

@SuppressWarnings("serial")
public class AccountWindow extends JFrame{
	
	private JLabel typeLabel;
	private JPanel p1, p2, pFin;
	private JButton b1, b2;
	
	public AccountWindow() {
		
		super("Account Type");
		p1 = new JPanel(); p2 = new JPanel(); pFin = new JPanel();
		typeLabel = new JLabel("Select the type of account:");
		b1 = new JButton("Saving"); b2 = new JButton("Spending");
		p1.add(typeLabel); p2.add(b1); p2.add(b2);
		
		pFin.setLayout(new BoxLayout(pFin, BoxLayout.PAGE_AXIS));
		pFin.add(p1); pFin.add(p2);
		add(pFin);
		
		setSize(250, 120);
		setResizable(false);
		setVisible(true);
	}
	
	public void setSavingListener(final Bank theBank, final View theView, final Person p) {
		
		b1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				theBank.addAccount(p, AccountType.SAVING);
				theView.refreshAccTable(theBank, p);
				
			}	
			
		});
		
	}
	
	public void setSpendingListener(final Bank theBank, final View theView, final Person p) {
		
		b2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				theBank.addAccount(p, AccountType.SPENDING);
				theView.refreshAccTable(theBank, p);
				
			}	
			
		});
		
	}
	
	public void setWindowCloseRefresh(WindowListener listenForClose) {
		
		addWindowListener(listenForClose);
		
	}

}
