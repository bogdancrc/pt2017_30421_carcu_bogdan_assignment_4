package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import PT2017.demo.Bank.Bank;
import PT2017.demo.Bank.BankDatabase;
import PT2017.demo.Bank.Person;


public class Controller {

	private View theView;
	private Bank theBank;
	
	public Controller(View v, Bank b) {
		
		theView = v;
		theBank = b;
		
		theView.setTableView(theBank);
		refreshMouse();
		
		
		theView.setAddPersonListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				PersonWindow personWindow = new PersonWindow();
				personWindow.setAddListener(theView, theBank);
				personWindow.setWindowCloseRefresh(new WindowAdapter() {
					
					 public void windowClosing(WindowEvent e) {
			               
						 	refreshMouse();
						 
			            }
					
				});
			}
			
		});
		
		theView.setAddAccountListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				
				int row = theView.getPersonTable().getSelectedRow();
				int index = (Integer)theView.getPersonTable().getValueAt(row, 0);
				Person p = theBank.findById(index);
				
				AccountWindow accountWindow = new AccountWindow();
				accountWindow.setSavingListener(theBank, theView, p);
				accountWindow.setSpendingListener(theBank, theView, p);
				accountWindow.setWindowCloseRefresh(new WindowAdapter() {
					
					public void windowClosing(WindowEvent e) {
			               
					 	refreshMouse();
					 
		            }
					
				});
		
			}
			
		});
		
		theView.setDeletePersonListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				int row = theView.getPersonTable().getSelectedRow();
				int index = (Integer)theView.getPersonTable().getValueAt(row, 0);
				Person p = theBank.findById(index);
				
				theBank.removePerson(p);
				theView.refreshTables(theBank);
				theView.refreshEmptyTable();
				refreshMouse();			}
			
		});
		
		theView.setDeleteAccountListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				int row = theView.getPersonTable().getSelectedRow();
				int index = (Integer)theView.getPersonTable().getValueAt(row, 0);
				Person p = theBank.findById(index);
				
				int row2 = theView.getAccountsTable().getSelectedRow();
				int index2 = (Integer)theView.getAccountsTable().getValueAt(row2, 0);
				
				theBank.deleteAccount(p, index2);
				theView.refreshAccTable(theBank, p);
				refreshMouse();
			}
			
		});
		
		theView.setDepositListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				int row = theView.getPersonTable().getSelectedRow();
				int index = (Integer)theView.getPersonTable().getValueAt(row, 0);
				Person p = theBank.findById(index);
				
				int row2 = theView.getAccountsTable().getSelectedRow();
				int index2 = (Integer)theView.getAccountsTable().getValueAt(row2, 0);
				
				try{
					theBank.deposit(p, index2, theView.getSumValue());
					theView.refreshAccTable(theBank, p);
				} catch (IllegalArgumentException ex) {
					
					theView.sendErrorMsg(ex.getMessage());
					
				} finally {
					
					refreshMouse();
				}
			
			}
			
		});
		
		theView.setWithdrawListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				int row = theView.getPersonTable().getSelectedRow();
				int index = (Integer)theView.getPersonTable().getValueAt(row, 0);
				Person p = theBank.findById(index);
				
				int row2 = theView.getAccountsTable().getSelectedRow();
				int index2 = (Integer)theView.getAccountsTable().getValueAt(row2, 0);
			
				try{
					theBank.withdraw(p, index2, theView.getWithdrawValue());
					theView.refreshAccTable(theBank, p);
				} catch (IllegalArgumentException ex) {
					
					theView.sendErrorMsg(ex.getMessage());
					
				} finally {
					
					refreshMouse();
				}
			}
			
		});
		
		theView.setGenerateListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				int row = theView.getPersonTable().getSelectedRow();
				int index = (Integer)theView.getPersonTable().getValueAt(row, 0);
				Person p = theBank.findById(index);
				
				int row2 = theView.getAccountsTable().getSelectedRow();
				int index2 = (Integer)theView.getAccountsTable().getValueAt(row2, 0);
				
				String s = theBank.generateReport(p, index2);
				new ReportWindow(s);
			}
				
			
		});
		
		theView.saveAllData(new WindowAdapter() {
			
			public void windowClosing(WindowEvent e) {
	               
			 	BankDatabase.saveData(theBank);
			 
            }
			
		});
	}
	
	private void refreshMouse() {
		
		theView.setShowAccountsListener(new MouseAdapter() {
			
			
			public void mouseClicked(MouseEvent event) {
	
				int row = theView.getPersonTable().rowAtPoint(event.getPoint());
				int index = (Integer)theView.getPersonTable().getValueAt(row, 0);
				Person p = theBank.findById(index);
				
				if(theBank.getMap().get(p).size() != 0)
					theView.refreshAccTable(theBank, p);
				else
					theView.refreshEmptyTable();
			}
			
			public void mousePressed(MouseEvent event) {
				
				int row = theView.getPersonTable().rowAtPoint(event.getPoint());
				int index = (Integer)theView.getPersonTable().getValueAt(row, 0);
				Person p = theBank.findById(index);
				
				if(theBank.getMap().get(p).size() != 0)
					theView.refreshAccTable(theBank, p);
				else
					theView.refreshEmptyTable();
			}
			
		});
		
	}
}
