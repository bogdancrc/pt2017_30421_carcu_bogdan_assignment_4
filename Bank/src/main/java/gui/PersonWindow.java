package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import PT2017.demo.Bank.Bank;
import PT2017.demo.Bank.Person;

@SuppressWarnings("serial")
public class PersonWindow extends JFrame{

	private JPanel p1, p2, p3, p4, pFin;
	private JLabel name, age, addr;
	private JTextField nameTxt, ageTxt, addrTxt;
	private JButton add;
	private Person heldPerson;
	
	public PersonWindow() {
		
		super("Person adder");
		p1 = new JPanel(); p2 = new JPanel(); p3 = new JPanel(); p4 = new JPanel(); pFin = new JPanel();
		name = new JLabel("Name:      ");
		age = new JLabel("Age:          ");
		addr = new JLabel("Address: ");
		nameTxt = new JTextField(10);
		ageTxt = new JTextField(10);
		addrTxt = new JTextField(10);
		add = new JButton("Add");
		
		pFin.setLayout(new BoxLayout(pFin, BoxLayout.PAGE_AXIS));
		p1.add(name); p1.add(nameTxt);
		p2.add(age); p2.add(ageTxt);
		p3.add(addr); p3.add(addrTxt);
		p4.add(add);
		pFin.add(p1); pFin.add(p2); pFin.add(p3); pFin.add(p4); 
		add(pFin);
		
		setSize(245, 200);
		setResizable(false);
		setVisible(true);
	}
	
	public void setAddListener(final View theView, final Bank bank) {
		
		add.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
			
				String name = nameTxt.getText();
				int age = Integer.parseInt(ageTxt.getText());
				String address = addrTxt.getText();
				
				heldPerson = new Person(name, age, address);
				bank.addPerson(heldPerson);
				theView.refreshTables(bank);
			}
				
		});
		
	}
	
	public void setWindowCloseRefresh(WindowListener listenForClose) {
		
		addWindowListener(listenForClose);
		
	}
	
	public Person getHeldPerson() {
		
		return heldPerson;
		
	}
	
}
