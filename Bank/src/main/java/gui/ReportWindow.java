package gui;

import javax.swing.JFrame;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class ReportWindow extends JFrame{
	
	private JTextArea area;
	
	public ReportWindow(String s) {
		
		super("Report");
		setSize(700, 100);
	
		area = new JTextArea();
		area.append(s);
		area.setEditable(false);
		
		add(area);
		setResizable(false);
		setVisible(true);
	}

}
