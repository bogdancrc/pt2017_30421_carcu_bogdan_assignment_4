package gui;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JTable;
import PT2017.demo.Bank.Account;
import PT2017.demo.Bank.Person;

public class TableBuilder {

	private TableBuilder() {
		
	}
	
	private static List<Person> getPersons(Map<Person, List<Account>> map) {
		
		List<Person> persons = new ArrayList<Person>();
		
		for(Person p : map.keySet())
			persons.add(p);
		
		return persons;
		
	}
	
	private static List<Account> getAccounts(Person p, Map<Person, List<Account>> map) {
		
		List<Account> accounts = map.get(p);
		return accounts;
		
	}
	
	private static JTable createPrsTable (List<Person> objects) {
		
		if(objects.size() == 0)
			return null;
	
		JTable result;	
		String[] headers = new String[objects.get(0).getClass().getDeclaredFields().length];
		Object[][] map = new Object[objects.size()][objects.get(0).getClass().getDeclaredFields().length];
		int i = 0, j = 0;
		
		for(int c = 0; c < headers.length; ++c) 
			headers[c] = objects.get(0).getClass().getDeclaredFields()[c].getName();
		
		for(Object o : objects) {
			for(Field field : o.getClass().getDeclaredFields()) {
				
				field.setAccessible(true);
				Object value;
				
				try{
					value = field.get(o);
					map[i][j++] = value;
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}	
			i++;
			j = 0;
		}
				
		result = new JTable(map, headers);
		
		return result;	
		
	}
	
	private static JTable createAccTable (List<Account> objects) {
		
		if(objects.size() == 0)
			return null;
	
		JTable result;	
		String[] headers = new String[objects.get(0).getClass().getSuperclass().getDeclaredFields().length];
		Object[][] map = new Object[objects.size()][objects.get(0).getClass().getSuperclass().getDeclaredFields().length];
		int i = 0, j = 0;
		
		for(int c = 0; c < headers.length; ++c) 
			headers[c] = objects.get(0).getClass().getSuperclass().getDeclaredFields()[c].getName();
		
		for(Object o : objects) {
			for(Field field : o.getClass().getSuperclass().getDeclaredFields()) {
				
				field.setAccessible(true);
				Object value;
				
				try{
					value = field.get(o);
					map[i][j++] = value;
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}	
			i++;
			j = 0;
		}
				
		result = new JTable(map, headers);
		return result;	
		
	}

	
	protected static JTable createPersonsTable(Map<Person, List<Account>> map) {
		
		return createPrsTable(getPersons(map));
		
	}
	
	protected static JTable createAccountsTable(Person p, Map<Person, List<Account>> map) {
		
		return createAccTable(getAccounts(p, map));
		
	}

}
