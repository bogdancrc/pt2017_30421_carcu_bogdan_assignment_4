package gui;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableColumn;

import PT2017.demo.Bank.Bank;
import PT2017.demo.Bank.Person;

@SuppressWarnings("serial")
public class View extends JFrame{
	
	private JPanel p1, p2, p3, p4, pFin;
	private JButton addP, deleteP, addAcc, deleteAcc, deposit, withdraw, generate; 
	private JLabel sum, wdraw;
	private JTextField sumTxt, wTxt;	
	private JTable personTable, accountsTable;
	private JScrollPane pane1, pane2;
	
	public View() {
		
		super("Blue Bank");
		
		p1 = new JPanel(); p2 = new JPanel(); p3 = new JPanel(); p4 = new JPanel(); pFin = new JPanel();
		addP = new JButton("Add Person"); deleteP = new JButton("Delete Person"); addAcc = new JButton("Add account");
		deleteAcc = new JButton("Delete account"); deposit = new JButton("Deposit"); withdraw = new JButton("Withdraw");
		generate = new JButton("Generate Report");
		sum = new JLabel("Sum: "); wdraw = new JLabel("              Withdraw : "); 
		sumTxt = new JTextField(8); wTxt = new JTextField(8);
		
		pFin.setLayout(new BoxLayout(pFin, BoxLayout.PAGE_AXIS));
		p1.add(addP); p1.add(addAcc); p1.add(deposit); p1.add(generate);
		p2.add(deleteP); p2.add(deleteAcc); p2.add(withdraw);
		p3.add(sum); p3.add(sumTxt); p3.add(wdraw); p3.add(wTxt);
		accountsTable = new JTable();
		pane2 = new JScrollPane(accountsTable);
		pane2.setPreferredSize(new Dimension(400, 300));
		
		pFin.add(p1); pFin.add(p2); pFin.add(p3); 
		
		setSize(850, 450);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
	}
	
	public void setTableView(Bank bank) {
		
		personTable = TableBuilder.createPersonsTable(bank.getMap());
		
		if(bank.getMap().entrySet().size() != 0) {
			TableColumn removable1 = personTable.getColumnModel().getColumn(0);
			TableColumn removable2 = personTable.getColumnModel().getColumn(1);
			personTable.removeColumn(removable1);
			personTable.removeColumn(removable2);
		}
		
		pane1 = new JScrollPane(personTable);
		pane1.setPreferredSize(new Dimension(400, 300)); 
		p4.add(pane1); 
		p4.add(pane2);
		pFin.add(p4);
		add(pFin);
		setVisible(true);
	}
	
	public void refreshTables(Bank bank) {
		
		p4.remove(pane1);
		p4.remove(pane2);
		
		personTable = TableBuilder.createPersonsTable(bank.getMap());
		
		if(bank.getMap().size() != 0) {
			TableColumn removable1 = personTable.getColumnModel().getColumn(0);
			TableColumn removable2 = personTable.getColumnModel().getColumn(1);
			personTable.removeColumn(removable1);
			personTable.removeColumn(removable2);
		}
		pane1 = new JScrollPane(personTable);
		pane1.setPreferredSize(new Dimension(400, 300)); 
		
		p4.add(pane1);
		p4.add(pane2);
		p4.repaint();
		p4.revalidate();

	}
	
	public void refreshAccTable(Bank bank, Person p) {
		
		p4.remove(pane2);
		accountsTable = TableBuilder.createAccountsTable(p, bank.getMap());
		
		if(bank.getMap().get(p).size() != 0) {
			TableColumn removable1 = accountsTable.getColumnModel().getColumn(0);
			TableColumn removable2 = accountsTable.getColumnModel().getColumn(1);
			accountsTable.removeColumn(removable1);
			accountsTable.removeColumn(removable2);
		}
		
		pane2 = new JScrollPane(accountsTable);
		pane2.setPreferredSize(new Dimension(400, 300)); 
		p4.add(pane2);
		p4.repaint();
		p4.revalidate();
		
	}
	
	public void refreshEmptyTable() {
		
		p4.remove(pane2);
		accountsTable = new JTable();
		pane2 = new JScrollPane(accountsTable);
		pane2.setPreferredSize(new Dimension(400, 300)); 
		p4.add(pane2);
		p4.repaint();
		p4.revalidate();
		
	}
	
	public void setAddPersonListener(ActionListener listenForButton) {
		
		addP.addActionListener(listenForButton);
		
	}
	
	public void setDeletePersonListener(ActionListener listenForButton) {
		
		deleteP.addActionListener(listenForButton);
		
	}
	
	public void setAddAccountListener(ActionListener listenForButton) {
		
		addAcc.addActionListener(listenForButton);
		
	}
	
	public void setDeleteAccountListener(ActionListener listenForButton) {
		
		deleteAcc.addActionListener(listenForButton);
		
	}
	
	public void setDepositListener(ActionListener listenForButton) {
		
		deposit.addActionListener(listenForButton);
		
	}
	
	public void setWithdrawListener(ActionListener listenForButton) {
		
		withdraw.addActionListener(listenForButton);
		
	}
	
	public void setShowAccountsListener(MouseListener listenForClick) {
		
		try{
			personTable.addMouseListener(listenForClick);
		} catch(NullPointerException e) {
			//nothing
		}
	}
	
	public void setGenerateListener(ActionListener listenForButton) {
		
		generate.addActionListener(listenForButton);
		
	}
	
	public void sendErrorMsg(String str) {
		
		JOptionPane.showMessageDialog(null, str, "Oops!", JOptionPane.ERROR_MESSAGE);
		
	}
	
	public JTable getPersonTable() {
		
		return personTable;
		
	}
	
	public JTable getAccountsTable() {
		
		return accountsTable;
		
	}
	
	public double getSumValue() {
		
		return Double.parseDouble(sumTxt.getText());
		
	}
	
	public double getWithdrawValue() {
		
		return Double.parseDouble(wTxt.getText());
		
	}
	
	public void saveAllData(WindowListener listenForClose) {
		
		addWindowListener(listenForClose);
		
	}
 
}
