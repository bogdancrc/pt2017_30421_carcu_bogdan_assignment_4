package PT2017.demo.Bank;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

///Test Driver

public class TestDriver {

	private Bank bank;
	private Person p;
	
	@Before
	public void prepareBank() {
		
		bank = new Bank();
		p = new Person("Eugen", 50, "Horea");
		
	}
	
	@Test
	public void testAddPerson() {

		bank.addPerson(p);
		assertTrue(bank.getMap().containsKey(p));
		
	}
	
	@Test
	public void testDeletePerson() {
		
		bank.addPerson(p);
		assertTrue(bank.removePerson(p));
		
	}
	
	@Test
	public void testAddAccount() {
		
		bank.addPerson(p);
		bank.addAccount(p, AccountType.SAVING);
		bank.addAccount(p, AccountType.SPENDING);
		List<Account> list = bank.getMap().get(p);
		
		assertEquals(list.size(), 2);
		
	}
	
	@Test 
	public void testDeleteAccount() {
		
		
		bank.addPerson(p);
		bank.addAccount(p, AccountType.SAVING);
		bank.addAccount(p, AccountType.SPENDING);
		
		assertTrue(bank.deleteAccount(p, 5));
		
		List<Account> list = bank.getMap().get(p);
		
		assertEquals(list.size(), 1);
		
	}
	
	@Test
	public void testDeposit() {
		
		bank.addPerson(p);
		bank.addAccount(p, AccountType.SAVING);
		bank.addAccount(p, AccountType.SPENDING);
		System.out.println(bank.getMap());
		bank.deposit(p, 7, 2001);
		bank.deposit(p, 8, 100);
		
		assertEquals((int) bank.getMap().get(p).get(0).getSum(), 2001);	
		assertEquals((int) bank.getMap().get(p).get(1).getSum(), 100);
		
	}
	
	@Test
	public void testWithdraw() {
		
		bank.addPerson(p);
		bank.addAccount(p, AccountType.SAVING);
		bank.addAccount(p, AccountType.SPENDING);
		
		bank.deposit(p, 1, 2001);
		bank.deposit(p, 2, 100);
		bank.withdraw(p, 1, 0); //does not matter how much we withdraw, the result is computed 
								//and the account is emptied.
		bank.withdraw(p, 2, 50);
		
		assertEquals((int) bank.getMap().get(p).get(0).getSum(), 0);
		assertEquals((int) bank.getMap().get(p).get(1).getSum(), 50);
		
	}
	
	
	@Test
	public void findTest() {
		
		bank.addPerson(p);
		assertNotSame(bank.findById(1), null);
		
	}
	

}
